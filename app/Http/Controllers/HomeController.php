<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\results;

class HomeController extends Controller
{
    public function inputs() {

        return view('index');
    }

    public function output($id) {

        $results = results::find($id);  //load in object from results table
        return view('out',compact('results'));
    }

    public function requests() {

        $total = results::all();
        return view('requests',compact('total'));
    }
    
    public function store() {

        
        //form validation checks if inputs are all numerical.
        $this->validate(request(), [
            'year' => 'integer',
            'Init' => 'numeric | required',
            'r' => 'numeric | required',
            'fee_rate' => 'numeric |required',
            'tax_rate' => 'numeric | required',
            'inf' => 'numeric | required'
        ]);

        //operate on inputs and store them into a table to be displayed
        $row = new \App\results; 

        $row->start_amt=request('Init');
        $row->return=request('r');
        $row->fee_type=request('fee_type');
        $row->fee_rate=request('fee_rate');
        $row->tax_rate=request('tax_rate');
        $row->inflation=request('inf');
        $row->years=request('years');
        
        $row->save();

        return redirect("/output/ $row->id ");
    }
}
