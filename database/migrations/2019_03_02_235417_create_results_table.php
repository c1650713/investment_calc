<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            //Create database for ouput table to be filled based on inputs
            $table->increments('id');
            $table->integer('years');
            $table->float('start_amt');
            $table->float('return');
            $table->boolean('fee_type');
            $table->float('tax_rate');
            $table->float('fee_rate');
            $table->float('inflation');
            $table->time('updated_at');
            $table->time('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
