<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bank Calculator</title>
    <style>
        body {
            background-color: darkgray;
            text-align: center;
            font-family:  'Arial';
        }
        table, th, td {
            border: 2px solid black;
            border-collapse: collapse;
            padding: 5px;
            align: center;
        }
        tr:hover {
            background-color: white;
        }
        table {
            width: 80%;
            margin: 0px auto;
        }
        input[type=number], input[type=float]{
            border: 2px solid black;
            border-radius: 4 px;
        }
        input[type=submit] {
            background-color: white;
            color: black;
            border: none;
            padding: 5px 10px;
            font-size: 150%;
        }
    </style>
</head>
<body>
    
    @yield('content')

</body>
</html>