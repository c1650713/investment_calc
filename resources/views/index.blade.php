@extends('master')

@section('content')
    
    <h1>Bank Investment Calculator</h1>
        
    <h2>
    Inputs
    </h2>
    
    <p>
        <b>Enter the following information and a table with predicted future values will be generated!</b>
        <br><br>
        Notes: Be sure to enter all percentages in decimal form. ex: 15 % = 0.15
        <br>
        All fields ARE required.  If an input is not needed, enter a value of 0 and it will be neglected in the calculations.
    </p>

    <form method=POST action='/output'>
        {{ csrf_field() }}
        initial amount: <br><input type="float" name="Init" min=0 required><br>

        annual return percent: <br><input type="float" name="r" min=0 max=1 required><br>

        bank fee type: <br>
        <input type="radio" name="fee_type" value="0" required> based on percentage of interest earned <br>
        <input type="radio" name="fee_type" value="1" required> based on percentage of invested amount <br><br>

        bank fee rate: <br><input type="float" name="fee_rate" min=0 max=1 required><br>

        tax rate on earnings:<br> <input type="float" name="tax_rate" min=0 max=1 required><br>

        inflation rate: <br> <input type="float" name="inf" min=0 max=1 required><br>

        number of years: <br> <input type="number" name="years" min=0 max=100 required><br><br>
        
        <input type="submit" value="submit">
    </form>

    <a href="/requests">Click here for a list of all requests</a><br>
    
    <div class="alert alert-error">  <!--  prints any errors in inputs from validation step when submit is selected -->
        <u1>
            @foreach($errors->all() as $error)
                <li>{{ $error }} </li>
            @endforeach
        </u1>
    </div>
@endsection