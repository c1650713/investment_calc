@extends('master')

@section('content')
    <h1 style="alignment: center">
    Predicted returns
    </h1>
    <p>
    This table includes calculated results with yearly compounding interest, for the number of years specified. <br>
    Balance adjusted for inflation is the future value of the account in terms of today's money.
    </p>

    <a href="/requests">Click here for a list of all requests</a><br>
    <a href="/">Click here to return to the start</a><br>
    <table> 
    <tr>
        <th>Year</th>
        <th>Starting balance</th>
        <th>Tax paid</th>
        <th>Bank fee paid</th>
        <th>Final balance</th>
        <th>Adjusted for inflation</th>
    </tr>

    <?php 
    $in_amt = $results['start_amt'];
    //for loop for each row
    for($x = 1;$x<=$results['years'];$x++)
    { 
        //new home of calculations
        if ($results['fee_type']) {
            $fee_amt = ($in_amt*$results['fee_rate']);

            $aft_inf = ($in_amt*(1-$results['fee_rate'])*(1+$results['return']*(1-$results['tax_rate'])));  //output calculation: A = P*(1-fee)*(1+r*(1-tax))
        }
        else {
            $fee_amt = ($in_amt*$results['return']*$results['fee_rate']); 

            $aft_inf = ($in_amt*(1+$results['return']*(1-$results['tax_rate']*(1-$results['fee_rate'])))); //output calculation: A = P*(1+r*(1-fee)*(1-tax))

        }
        $bef_inf = ($aft_inf)/(1+$results['inflation']); //adjustment for inflation: Anew = A/(1+inflationRate)
        $tax_amt = $in_amt*$results['return']*$results['tax_rate'];
        //print each row
?>
    <tr> 
        <td><?php echo round($x,2); ?></td>
        <td><?php echo round($in_amt,2); ?></td>
        <td><?php echo round($tax_amt,2); ?></td>
        <td><?php echo round($fee_amt,2); ?></td>
        <td><?php echo round($aft_inf,2); ?></td>
        <td><?php echo round($bef_inf,2); ?></td>
    </tr> 
<?php
        $in_amt = $aft_inf;
    } ?>
    
    </table>


@endsection
